# Taller: Introducción a GitLab Pipelines

## Requisitos:
1. Tener o crear una cuenta en [GitLab](https://gitlab.com/).
2. Tener **Git** instalado en tu máquina.
3. Tener **Python** instalado (Opcional).

---

## Pasos en GitLab:

1. **Crear un proyecto en GitLab:**
   - Ve a tu cuenta en GitLab y crea un nuevo proyecto.
   
2. **Clonar el repositorio localmente:**
    ```bash
    git clone <url-del-repo>

3. **Crear una rama diferente a main y posicionarse en ella:**
    ```bash
    git branch <nombre-de-rama>
    git checkout <nombre-de-rama>
    O bien
    git checkout -b <nombre-de-rama>

4. **Hacer cambios en el repositorio localmente:**
- Realiza los cambios que desees en los archivos del proyecto.

5. **Hacer push de los cambios a GitLab:**
    ```bash
    git add .
    git commit -m "Descripción de los cambios"
    git push origin <nombre-de-rama>
    
6. **Crear un Merge Request desde GitLab:**
- Desde la interfaz de GitLab, crea un Merge Request desde tu rama hacia main.

7. **Completar el Merge Request:**
- Revisa los cambios, asegúrate de que todo esté correcto y completa el merge.