import pytest

from src.factorial import factorial


@pytest.mark.parametrize('n, result, exception', [
    (0, 1, False),
    (1, 1, False),
    (2, 2, False),
    (3, 6, False),
    (4, 24, False),
    (-1, "received negative input", pytest.raises(ValueError))
])
def test_factorial(n, result, exception):
    if exception:
        with exception:
            assert factorial(n) == result
    else:
        assert factorial(n) == result


def test_factorial_exception():
    with pytest.raises(ValueError):
        assert factorial(-1) == "received negative input"


@pytest.mark.parametrize("n", [
    3.5
])
def test_factorial_invalid_input(n):
    with pytest.raises(TypeError):
        factorial(n)
