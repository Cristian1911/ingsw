import pytest
from src.calculator import add, substract, multiply, divide


@pytest.mark.parametrize('a,b,expected', [
    (10, 20, 30),
    (15, 25, 40),
])
def test_add(a, b, expected):
    result = add(a, b)
    assert result == expected
