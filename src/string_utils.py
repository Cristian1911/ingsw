# string_utils.py

def reverse_string(s):
    return s[::-1]

def capitalize_string(s):
    return s.capitalize()

def is_palindrome(s):
    cleaned = ''.join(e for e in s if e.isalnum()).lower()
    return cleaned == cleaned[::-1]
